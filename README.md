# Matemáticas

## 1. Conjuntos, Aplicaciones y funciones (2002)

```plantuml
@startmindmap
<style>
mindmapDiagram {
  Linecolor black
  .titulo {
    BackgroundColor #A9DCDF
  }
  .sub1 {
    BackgroundColor #B4A7E5
  }
  .sub2 {
    BackgroundColor #ADD1B2
  }
}
</style>

* Conjuntos, Aplicaciones y funciones <<titulo>>

** Conjuntos <<sub1>>

***_ a partir de estos \n nace la
**** Teoria de conjuntos<<sub2>>
*****_ piezas basicas
****** Conjunto y Elemento
*******_ es una
******** Idea intuitiva
*********_ es
********** Racional
**********_ y
*********** Abstracta
*******_ tienen una
******** Relacion de pertenencia
****** Inclusion
*******_ un
******** Conjunto
*********_ esta 
********** Incluido
***********_ en otro, \n cuando todos
************ Los elementos
*************_ del  
************** Primer Conjunto
***************_ pertenecen al
**************** Segundo conjunto
***** Operaciones
******_ basicas
******* Interseccion
********_ son
********* Elementos
**********_ que 
*********** Pertenecen
************ Simultaneamente 
*************_ a ambos
************** Conjuntos
******* Union
********_ son
********* Elementos
**********_ que 
*********** Pertenecen
************_ al menos a \n uno de los
************* Conjuntos
******* Complementacion
********_ son
********* Elementos
**********_ que 
*********** No Pertenecen
************_ y son
************* Complementarios
**************_ de un
*************** Conjunto dado

***** Mezcla
******_ de las 
******* Operaciones \nbasicas
********_ surge la
********* Diferencia
**********_ de
*********** Conjuntos

******* Universal 
********_ es un
********* Conjunto 
**********_ de
*********** Referencia
************_ donde ocurren \n cosas de la
************* Teoria

******* Vacio
********_ es una
********* Necesidad 
********** Logica
***********_ para
************ Representar
*************_ el
************** Conjunto
***************_ que
**************** No tiene
***************** Elementos 

***** Representacion
******_ mediante 
******* Figuras
********_ que
********* Encierran 
**********_ a los
*********** Elementos
************_ que
************* Forman
**************_ el
*************** Conjunto
******_ se deben al
******* Logico
********_ llamado
********* John Venn
**********_ Por lo que \n se conocen como
*********** Diagramas de Venn

***** Cardinalidad
******_ une la
******* Teoria de los numeros naturales
********_ con la
********* Teoria de conjuntos
******_ hace
******* referencia 
********_ a el
********* Numero
**********_ de
*********** Elementos
************_ que
************* Conforman
**************_ un
*************** Conjunto
******_ existen varias
******* Formulas 
********_ Como
********* Cardinal de la union de conjuntos
********* Acotacion de cardinales

** Aplicaciones <<sub1>>

***_ es una forma de
**** Transformacion
*****_ a través de la cual se
****** Identifica
*******_ que
******** Elemento
*********_ de un
********** Primer conjunto
***********_ se
************ Convierte
*************_ en un
************** Segundo conjunto
*****_ que
****** Convierte
*******_ a cada uno \n de los
******** Elementos
*********_ de un
********** Determinado conjunto \n (inicial)
***********_ en un 
************ Unico Elemento
*************_ de un
************** Conjunto final \n (destino)

*****_ todos los
****** Elementos
*******_ del
******** Conjunto
*********_ deben tener un
********** Transformado
*********** Unico

***_ existen distintos
**** Tipos
*****_ como
****** Aplicacion Inyectiva
****** Aplicacion Subyectiva
****** Aplicacion Biyectiva
**** Ideas
***** Abstractas
******_ como
******* Imagen 
********_ e
********* Imagen inversa
******* Transformacion de transformacion
******* Composicion 
********_ de
********* Aplicaciones

** Funciones <<sub1>>
***_ representa un
**** Conjunto 
*****_ de
****** Valores 
*******_ se pueden
******** Representar
*********_ de manera
********** Grafica
***********_ son
************ Faciles
*************_ de
************** Ver
*******_ son una
******** Serie de puntos
*********_ en un
********** Sistema de coordenadas
***********_ que nos
************ Generan
*************_ una
************** Figura
***************_ pueden ser
**************** Linea recta
**************** Parabolas
**************** Figuras mas complejas, etc


@endmindmap 
```


## 2. Funciones (2010)

```plantuml
@startmindmap
<style>
mindmapDiagram {
  Linecolor black
  .titulo {
    BackgroundColor #A9DCDF
  }
  .sub1 {
    BackgroundColor #B4A7E5
  }
  .sub2 {
    BackgroundColor #ADD1B2
  }
}
</style>

* Funciones <<titulo>>

**_ son un
*** Conjunto de numeros <<sub1>>
****_ que pueden ser
***** Representados
******_ de manera
******* Grafica
********_ mediante el uso del
********* Plano Cartesiano
******_ utilizando
******* Eje X
******** x
******* Eje y
******** Imagen f(x)
** Caracteristicas <<sub1>>
*** Crecientes <<sub2>>
****_ cuando aumenta la
***** Variable independiente
******_ tambien aumentan sus
******* Imagenes
*** Derecientes <<sub2>>
****_ cuando aumenta la
***** Variable independiente
******_ disminuyen sus
******* Imagenes
*** Maximos y minimos \n relativos <<sub2>>
****_ se refiere a los
***** Valores
****** Mayores 
****** Menores
******_ que puede tomar una
******* Funcion
********_ en un
********* Intervalo determinado
*** Limites <<sub2>>
****_ se refiere a 
***** Los valores 
******_ de la
******* Variable X
********_ que estan cerca de un
********* Determinado punto
**********_ y a
*********** Los valores 
************_ de la
************* Variable Y
**************_ que estan cerca de un
*************** Determinado valor
****************_ son
***************** Continuas
****_ idea principal, a que 
***** Valor 
******_ se aproximan los
******* Valores
********_ de una
********* Funcion
**********_ cuando nos 
*********** Acercamos
************_ al
************* Punto de interes
****_ no existen, cuando la
***** Funcion
******_ tiende a
******* Cero

*** Continuidad <<sub2>>
****_ no produce
***** Saltos
******_ ni
******* Discontinuidades
********_ son
********* Funciones
**********_ que se
*********** Comportan mejor
************_ son mas
************* Manejables

*** Calculo Diferencial <<sub2>>
**** Derivada
*****_ es la
****** Pendiente
*******_ de la
******** Recta tangente
*****_ permite
****** Resolver
*******_ el problema de la
******** Aproximacion
*********_ de una 
********** Funcion compleja
***********_ mediante una
************ Funcion lineal simple

@endmindmap 
```

## 3. La matemática del computador (2002)

```plantuml
@startmindmap
<style>
mindmapDiagram {
  Linecolor black
  .titulo {
    BackgroundColor #A9DCDF
  }
  .sub1 {
    BackgroundColor #B4A7E5
  }
  .sub2 {
    BackgroundColor #ADD1B2
  }
}
</style>

* La matemática del computador <<titulo>>

** Aritmetica Finita <<sub1>>
*** Problemas <<sub2>>
**** Numero aproximado
***** Error aproximacion
******_ en numeros con
******* Numero infinito de cifras
**** Truncar
*****_ es 
****** Cortar
*******_ un 
******** Numero
*********_ con
********** Infinitas cifras a \n la derecha
**** Redondear
*****_ es un 
****** Truncamiento refinado
*******_ hacer que el
******** Error
*********_ sea lo mas
********** Pequeño posible

** Sistemas numericos<<sub1>>
*** Representaciones compactas <<sub2>>
****_ de numeros
***** Sistema octal
******_ Se utilizan 
******* Menos simbolos
***** Sistema hexadecimal
******_ Se utilizan 
******* Menos simbolos
***** Sistema binario <<sub2>>
******_ se pueden representar
******* Circuitos de corriente
********_ los estados del
********* Paso de corriente
********** Encendido (1)
********** Apagado(0)
******* Aritmetica flotante
******** Truncamiento
******** Redondeo
******* Numeros pequenos
******* Numeros grandes
******** Notacion cientifica
******_ sistema de
******* Base 2
******** Logica binaria
*********_ de los
********** Ordenadores




@endmindmap 
```
